from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('orders',views.OrderListView.as_view(), name='orders'),
    path('orders/<int:pk>/', views.OrderView.as_view(), name='single_order'),
    path('create-order', views.create_order, name='create_order'),
    path('order-created', views.order_handler, name='order_created'),
]

