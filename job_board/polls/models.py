from django.db import models

# Create your models here.
class User(models.Model):
    full_name = models.CharField(max_length=90)
    registration_date = models.DateField()
    def __str__(self):
        return self.full_name

class Order(models.Model):
    create_date = models.DateField()
    performer_id = models.ForeignKey(User, on_delete = models.PROTECT)
    category = models.CharField(max_length=200)
    is_closed = models.BooleanField(null=True)
    is_done = models.BooleanField(null=True)
    done_date = models.DateField(null=True)
    title = models.CharField(max_length=60)
    price = models.IntegerField(null=True)
    text = models.TextField()
    def __str__(self):
        return self.title

class Response(models.Model):
    performer_id = models.ForeignKey(User, on_delete = models.PROTECT, related_name='performer')
    customer_id = models.ForeignKey(User, on_delete = models.PROTECT, related_name='customer')
    order_id = models.ForeignKey(Order, on_delete = models.PROTECT)
    response_date = models.DateField()
    def get_order_title(self):
        return Order.objects.filter(id=self.order_id_id)[0].title
