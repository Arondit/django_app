from django.shortcuts import get_object_or_404,render
from django.views import generic
from django.http import HttpResponse

from .models import Order

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    def get_queryset(self):
        return None

class OrderListView(generic.ListView):
    model = Order
    context_object_name = 'orders'
    def get_queryset(self):
        return Order.objects.order_by('-create_date')

class OrderView(generic.DetailView):
    model = Order
    context_object_name = 'order'
    def get_queryser(self):
        return get_object_or_404(Order, pk=order_id)


def create_order(request):
    return render(request, 'polls/create_order.html')

def order_handler(request):
    return HttpResponse(f'Order is not created, page is not ready. Type is {request.method}')